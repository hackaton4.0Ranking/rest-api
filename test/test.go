// File to try some tie cases to sort election candidates based on result
// clear && go run test.go | jq
package main

import (
	"encoding/json"
	"fmt"
	"slices"
)

type Election struct {
	Candidates []map[string]interface{}
}

func main() {
	// // tie
	// election := Election{
	// 	Candidates: []map[string]interface{}{
	// 		{
	// 			"title":       "candidate A1",
	// 			"description": "first candidate",
	// 			"count": []int{0, 3, 1, 2, 3},
	// 		},
	// 		{
	// 			"title":       "candidate A",
	// 			"description": "first candidate",
	// 			"count": []int{0, 1, 1, 1, 1},
	// 		},
	// 		{
	// 			"title":       "candidate B",
	// 			"description": "second candidate",
	// 			"count": []int{3, 1, 2, 0, 0},
	// 		},
	// 		{
	// 			"title":       "candidate C",
	// 			"description": "third candidate",
	// 			"count": []int{3, 0, 2, 1, 0},
	// 		},
	// 		{
	// 			"title":       "candidate D",
	// 			"description": "forth candidate",
	// 			"count": []int{0, 1, 0, 2, 2},
	// 		},
	// 	},
	// }

	// tie
	election := Election{
		Candidates: []map[string]interface{}{
			{
				"title":       "candidate A1",
				"description": "first candidate",
				"count": []int{0, 0, 3, 0, 6},
			},
			{
				"title":       "candidate A",
				"description": "first candidate",
				"count": []int{0, 1, 1, 2, 0},
			},
			{
				"title":       "candidate B",
				"description": "second candidate",
				"count": []int{0, 1, 1, 4, 0},
			},
			{
				"title":       "candidate C",
				"description": "third candidate",
				"count": []int{3, 2, 1, 0, 0},
			},
			{
				"title":       "candidate D",
				"description": "forth candidate",
				"count": []int{3, 2, 1, 0, 0},
			},
		},
	}

	slices.SortFunc(election.Candidates, func(a, b map[string]interface{}) int {
		counta := a["count"].([]int)
		countb := b["count"].([]int)
		for i := 0; i < len(counta) - 1; i++ {
			if counta[i] > countb[i] || counta[len(counta)-1] < countb[len(countb)-1] {
				return -1
			} else if counta[i] < countb[i] || counta[len(counta)-1] > countb[len(countb)-1] {
				return 1
			}
		}
		return 0
	})

	j, _ := json.MarshalIndent(election.Candidates, "", "\t")
	fmt.Println(string(j))
}