# Hackaton 4.0 API REST

## Execution
.env for sqlite
```toml
# sqlite live
DB_DRIVER=sqlite3
API_SECRET=98hbun98h #Used for creating a JWT. Can be anything 
DB_NAME=fullstack.sqlite
# email sender
SMTP_HOST=smtp.gmail.com
SMTP_PORT=587
EMAIL_FROM=example@gmail.com
EMAIL_PASS=password
# frontend info
FRONTEND_BASEURL=https://ranking.example.com
FRONTEND_LOGIN_PATH=/login?token=
```
then run:
```
go mod tidy
go run main.go
```

## Go-JWT-Postgres-Mysql-Restful-API
This is an application built with golang, jwt, gorm, postgresql, mysql.

You can follow the guide here:
https://levelup.gitconnected.com/crud-restful-api-with-go-gorm-jwt-postgres-mysql-and-testing-460a85ab7121

### Dockerizing the API
The dockerized API can be found here:
https://github.com/victorsteven/Dockerized-Golang-Postgres-Mysql-API
