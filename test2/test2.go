package main

import (
	"fmt"
)

type Ballot struct {
	Preferences []string
}

func instantRunoffElection(ballots []Ballot) []string {
	// Tally the first preferences
	voteCount := tallyFirstPreferences(ballots)
	totalVotes := len(ballots)
	eliminationOrder := []string{}

	for {
		// Check if any candidate has a majority
		for candidate, count := range voteCount {
			if count > totalVotes/2 {
				eliminationOrder = append(eliminationOrder, candidate)
				return eliminationOrder
			}
		}

		// Find the candidate with the fewest votes
		minVotes := totalVotes + 1
		var candidateToEliminate string
		for candidate, count := range voteCount {
			if count < minVotes {
				minVotes = count
				candidateToEliminate = candidate
			}
		}

		// Record the eliminated candidate
		eliminationOrder = append(eliminationOrder, candidateToEliminate)

		// Eliminate the candidate and redistribute their votes
		voteCount = redistributeVotes(candidateToEliminate, ballots, voteCount)
	}
}

func tallyFirstPreferences(ballots []Ballot) map[string]int {
	voteCount := make(map[string]int)
	for _, ballot := range ballots {
		if len(ballot.Preferences) > 0 {
			firstChoice := ballot.Preferences[0]
			voteCount[firstChoice]++
		}
	}
	return voteCount
}

func redistributeVotes(eliminatedCandidate string, ballots []Ballot, voteCount map[string]int) map[string]int {
	newVoteCount := make(map[string]int)
	for candidate, count := range voteCount {
		if candidate != eliminatedCandidate {
			newVoteCount[candidate] = count
		}
	}

	for _, ballot := range ballots {
		for i, candidate := range ballot.Preferences {
			if candidate == eliminatedCandidate {
				for j := i + 1; j < len(ballot.Preferences); j++ {
					nextChoice := ballot.Preferences[j]
					if _, stillInRace := voteCount[nextChoice]; stillInRace {
						newVoteCount[nextChoice]++
						break
					}
				}
				break
			}
		}
	}

	return newVoteCount
}

func main() {
	ballots := []Ballot{
		{Preferences: []string{"Charlie", "Alice", "Bob"}},
		{Preferences: []string{"Bob", "Charlie", "Alice"}},
		{Preferences: []string{"Bob", "Alice", "Charlie"}},
		{Preferences: []string{"Alice", "Bob", "Charlie"}},
		{Preferences: []string{"Alice", "Charlie", "Bob"}},
		{Preferences: []string{"Charlie", "Alice", "Bob"}},
		{Preferences: []string{"Charlie", "Bob", "Alice"}},
		{Preferences: []string{"Charlie", "Alice", "Bob"}},
		{Preferences: []string{"Bob", "Charlie", "Alice"}},
	}

	ranking := instantRunoffElection(ballots)
	fmt.Printf("Final ranking (from first to last):\n")
	for i, candidate := range ranking {
		fmt.Printf("%d. %s\n", i+1, candidate)
	}
}
