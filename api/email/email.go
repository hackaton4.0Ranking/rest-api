package email

import (
	"log"
	"net/smtp"
	"os"
)

type Email struct {
	To string
	Subject string
	Body string
}

func (e *Email) Send() error {
  from := os.Getenv("EMAIL_FROM")

  msg := "From: " + from + "\n" +
    "To: " + e.To + "\n" +
    "Subject: " + e.Subject + "\n\n" +
    e.Body

  err := smtp.SendMail(
    os.Getenv("SMTP_HOST") + ":" + os.Getenv("SMTP_PORT"),
    smtp.PlainAuth("", from, os.Getenv("EMAIL_PASS"), os.Getenv("SMTP_HOST")),
    from,
    []string{e.To},
    []byte(msg),
  )

  if err != nil {
    log.Printf("smtp error: %s", err)
    return err
  }
  log.Println("Successfully sended to " + e.To)
  return nil
}
