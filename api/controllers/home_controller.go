package controllers

import (
	"net/http"

	"gitlab.com/hackaton4.0Ranking/rest-api/api/responses"
)

func (server *Server) Home(w http.ResponseWriter, r *http.Request) {
	responses.JSON(w, http.StatusOK, "Hackaton Ranking API v1.0.0")

}
