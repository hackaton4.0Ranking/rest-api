package controllers

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"slices"
	"strconv"

	"github.com/gorilla/mux"
	"gitlab.com/hackaton4.0Ranking/rest-api/api/auth"
	"gitlab.com/hackaton4.0Ranking/rest-api/api/models"
	"gitlab.com/hackaton4.0Ranking/rest-api/api/responses"
	"gitlab.com/hackaton4.0Ranking/rest-api/api/utils/formaterror"
)

func (server *Server) CreateElection(w http.ResponseWriter, r *http.Request) {

	body, err := io.ReadAll(r.Body)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}
	election := models.Election{}
	err = json.Unmarshal(body, &election)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}
	election.Prepare()
	err = election.Validate()
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}
	uid, _, err := auth.ExtractTokenID(r)
	if err != nil {
		responses.ERROR(w, http.StatusUnauthorized, errors.New("Unauthorized"))
		return
	}
	if uid != election.AuthorID {
		responses.ERROR(w, http.StatusUnauthorized, errors.New(http.StatusText(http.StatusUnauthorized)))
		return
	}
	electionCreated, err := election.SaveElection(server.DB)
	if err != nil {
		formattedError := formaterror.FormatError(err.Error())
		responses.ERROR(w, http.StatusInternalServerError, formattedError)
		return
	}
	w.Header().Set("Location", fmt.Sprintf("%s%s/%d", r.Host, r.URL.Path, electionCreated.ID))
	responses.JSON(w, http.StatusCreated, electionCreated)
}

func (server *Server) GetElections(w http.ResponseWriter, r *http.Request) {

	election := models.Election{}

	elections, err := election.FindAllElections(server.DB)
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	responses.JSON(w, http.StatusOK, elections)
}

func (server *Server) GetElection(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	pid, err := strconv.ParseUint(vars["id"], 10, 64)
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}
	election := models.Election{}

	electionReceived, err := election.FindElectionByID(server.DB, pid)
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	responses.JSON(w, http.StatusOK, electionReceived)
}

func (server *Server) GetElectionResult(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	pid, err := strconv.ParseUint(vars["id"], 10, 64)
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}
	election := &models.Election{}

	election, err = election.FindElectionByID(server.DB, pid)
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}

	vote := models.Vote{
		ElectionID: pid,
	}
	votes, err := vote.FindAllVotes(server.DB, false)
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}

	for _, vote := range *votes {
		for pos, candidate := range vote.Ranking {
			index := slices.IndexFunc(
				election.Candidates,
				func(c map[string]interface{}) bool {
					return c["title"] == candidate["title"]
				},
			)
			if index != -1 {
				if election.Candidates[index]["count"] == nil {
					election.Candidates[index]["count"] = make([]int, len(election.Candidates))
				}
				count := election.Candidates[index]["count"].([]int)
				count[pos] += 1
			}
		}
	}

	slices.SortFunc(election.Candidates, func(a, b map[string]interface{}) int {
		counta := a["count"].([]int)
		countb := b["count"].([]int)
		for i := 0; i < len(counta) - 1; i++ {
			if counta[i] > countb[i] || counta[len(counta)-1] < countb[len(countb)-1] {
				return -1
			} else if counta[i] < countb[i] || counta[len(counta)-1] > countb[len(countb)-1] {
				return 1
			}
		}
		return 0
	})

	responses.JSON(w, http.StatusOK, election)
}

func (server *Server) UpdateElection(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)

	// Check if the election id is valid
	pid, err := strconv.ParseUint(vars["id"], 10, 64)
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}

	//CHeck if the auth token is valid and  get the user id from it
	uid, _, err := auth.ExtractTokenID(r)
	if err != nil {
		responses.ERROR(w, http.StatusUnauthorized, errors.New("Unauthorized"))
		return
	}

	// Check if the election exist
	election := models.Election{}
	err = server.DB.Debug().Model(models.Election{}).Where("id = ?", pid).Take(&election).Error
	if err != nil {
		responses.ERROR(w, http.StatusNotFound, errors.New("election not found"))
		return
	}

	// If a user attempt to update a election not belonging to him
	if uid != election.AuthorID {
		responses.ERROR(w, http.StatusUnauthorized, errors.New("Unauthorized"))
		return
	}
	// Read the data posted
	body, err := io.ReadAll(r.Body)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	// Start processing the request data
	electionUpdate := models.Election{}
	err = json.Unmarshal(body, &electionUpdate)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	//Also check if the request user id is equal to the one gotten from token
	if uid != electionUpdate.AuthorID {
		responses.ERROR(w, http.StatusUnauthorized, errors.New("Unauthorized"))
		return
	}

	electionUpdate.Prepare()
	err = electionUpdate.Validate()
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	electionUpdate.ID = election.ID //this is important to tell the model the election id to update, the other update field are set above

	electionUpdated, err := electionUpdate.UpdateAElection(server.DB)

	if err != nil {
		formattedError := formaterror.FormatError(err.Error())
		responses.ERROR(w, http.StatusInternalServerError, formattedError)
		return
	}
	responses.JSON(w, http.StatusOK, electionUpdated)
}

func (server *Server) DeleteElection(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)

	// Is a valid election id given to us?
	pid, err := strconv.ParseUint(vars["id"], 10, 64)
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}

	// Is this user authenticated?
	uid, _, err := auth.ExtractTokenID(r)
	if err != nil {
		responses.ERROR(w, http.StatusUnauthorized, errors.New("Unauthorized"))
		return
	}

	// Check if the election exist
	election := models.Election{}
	err = server.DB.Debug().Model(models.Election{}).Where("id = ?", pid).Take(&election).Error
	if err != nil {
		responses.ERROR(w, http.StatusNotFound, errors.New("Unauthorized"))
		return
	}

	// Is the authenticated user, the owner of this election?
	if uid != election.AuthorID {
		responses.ERROR(w, http.StatusUnauthorized, errors.New("Unauthorized"))
		return
	}
	_, err = election.DeleteAElection(server.DB, pid, uid)
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}
	w.Header().Set("Entity", fmt.Sprintf("%d", pid))
	responses.JSON(w, http.StatusNoContent, "")
}
