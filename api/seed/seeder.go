package seed

import (
	"log"

	"gitlab.com/hackaton4.0Ranking/rest-api/api/models"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

var users = []models.User{
	{
		Nickname: "Steven victor",
		Email:    "steven@gmail.com",
		Password: "password",
		Admin:    true,
	},
	{
		Nickname: "Martin Luther",
		Email:    "luther@gmail.com",
		Password: "password",
	},
}

var posts = []models.Post{
	{
		Title:    "Title 1",
		Content:  "Hello world 1",
		AuthorID: 1,
	},
	{
		Title:    "Title 2",
		Content:  "Hello world 2",
		AuthorID: 1,
	},
}

var elections = []models.Election{
	{
		Title:       "Title 1",
		Description: "Hello world 1",
		Candidates: []map[string]interface{}{
			{
				"title":       "candidate A",
				"description": "first candidate",
			},
			{
				"title":       "candidate B",
				"description": "second candidate",
			},
			{
				"title":       "candidate C",
				"description": "third candidate",
			},
			{
				"title":       "candidate D",
				"description": "forth candidate",
			},
		},
		AuthorID: 1,
	},
	{
		Title:       "Title 2",
		Description: "Hello world 2",
		AuthorID:    1,
		Candidates: []map[string]interface{}{
			{
				"title":       "candidate 1",
				"description": "first candidate",
			},
			{
				"title":       "candidate 2",
				"description": "second candidate",
			},
			{
				"title":       "candidate 3",
				"description": "third candidate",
			},
			{
				"title":       "candidate 4",
				"description": "forth candidate",
			},
		},
		Data: models.JSON{
			"start": "2024-05-04T15:11:59Z",
			"end":   "2024-05-04T20:11:59Z",
		},
	},
}

func Load(db *gorm.DB) {
	err := db.Debug().AutoMigrate(&models.User{}, &models.Post{}, &models.Election{}, &models.Vote{})
	if err != nil {
		log.Fatalf("cannot migrate table: %v", err)
	}

	/*
		err = db.Debug().Model(&models.Post{}).AddForeignKey("author_id", "users(id)", "cascade", "cascade").Error
		if err != nil {
			log.Fatalf("attaching foreign key error: %v", err)
		}
	*/

	for i := range users {
		err = db.Debug().Clauses(clause.OnConflict{
			DoNothing: true,
		}).Model(&models.User{}).Create(&users[i]).Error
		if err != nil {
			log.Fatalf("cannot seed users table: %v", err)
		}
		if posts[i].AuthorID == 0 {
			posts[i].AuthorID = users[i].ID
		}

		err = db.Debug().Clauses(clause.OnConflict{
			DoNothing: true,
		}).Model(&models.Post{}).Create(&posts[i]).Error
		if err != nil {
			log.Printf("cannot seed posts table: %v\n", err)
		}

		err = db.Debug().Clauses(clause.OnConflict{
			DoNothing: true,
		}).Model(&models.Election{}).Create(&elections[i]).Error
		if err != nil {
			log.Printf("cannot seed elections table: %v\n", err)
		}
	}
}
