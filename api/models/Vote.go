package models

import (
	"errors"
	"slices"
	"time"

	"gorm.io/gorm"
)

type Vote struct {
	ID          uint64    `gorm:"primary_key;auto_increment" json:"id"`
	ElectionID  uint64    `gorm:"column:election_id" json:"election_id"`
	Author      User      `json:"author"`
	AuthorID    uint32    `sql:"column:author_id;type:int REFERENCES users(id)" json:"author_id"`
	Ranking     JsonArray `json:"ranking"`
	CreatedAt   time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
}

func (p *Vote) Prepare() {
	p.ID = 0
	p.Author = User{}
	p.CreatedAt = time.Now()
}

func (p *Vote) Validate() error {
	if p.ElectionID < 1 {
		return errors.New("required Election")
	}
	if p.AuthorID < 1 {
		return errors.New("required Author")
	}
	if len(p.Ranking) < 1 {
		return errors.New("required Candidates")
	}
	for i, c1 := range p.Ranking {
		for j, c2 := range p.Ranking {
			if c1["title"] == c2["title"] && i != j {
				return errors.New("required Ranking with unique title")
			}
		}
	}
	return nil
}

func (v *Vote) SaveVote(db *gorm.DB) (*Vote, error) {
	var err error
	var existing Vote
	election := &Election{}
	election, err = election.FindElectionByID(db, v.ElectionID)
	if err != nil {
		return &Vote{}, err
	}

	err = db.Debug().Model(&Vote{}).Where("election_id = ? and author_id = ?", v.ElectionID, v.AuthorID).First(&existing).Error
	if err == nil || existing.ID != 0 {
		return &Vote{}, errors.New("vote already exists")
	}

	if len(v.Ranking) != len(election.Candidates) {
		return &Vote{}, errors.New("required full candidate ranking")
	}

	for _, cRank := range v.Ranking {
		index := slices.IndexFunc(election.Candidates, func(c map[string]interface{}) bool { return c["title"] == cRank["title"] })
		if index < 0 {
			return &Vote{}, errors.New("required full candidate list from election")
		}
	}

	err = db.Debug().Model(&Vote{}).Create(&v).Error
	if err != nil {
		return &Vote{}, err
	}
	if v.ID != 0 {
		err = db.Debug().Model(&User{}).Where("id = ?", v.AuthorID).Omit("password").Take(&v.Author).Error
		if err != nil {
			return &Vote{}, err
		}
	}
	return v, nil
}

func (v *Vote) FindAllVotes(db *gorm.DB, getAuthors bool) (*[]Vote, error) {
	var err error
	votes := []Vote{}
	err = db.Debug().Model(&Vote{}).Where("election_id = ? or ? = 0", v.ElectionID, v.ElectionID).Find(&votes).Error
	if err != nil {
		return &[]Vote{}, err
	}
	if len(votes) > 0 && getAuthors {
		for i := range votes {
			err := db.Debug().Model(&User{}).Where("id = ?", votes[i].AuthorID).Omit("password").Take(&votes[i].Author).Error
			if err != nil {
				return &[]Vote{}, err
			}
		}
	}
	return &votes, nil
}

func (v *Vote) FindVoteByID(db *gorm.DB, pid uint64) (*Vote, error) {
	var err error
	err = db.Debug().Model(&Vote{}).Where("id = ?", pid).Take(&v).Error
	if err != nil {
		return &Vote{}, err
	}
	if v.ID != 0 {
		err = db.Debug().Model(&User{}).Where("id = ?", v.AuthorID).Omit("password").Take(&v.Author).Error
		if err != nil {
			return &Vote{}, err
		}
	}
	return v, nil
}