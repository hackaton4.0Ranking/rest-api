package models

import (
	"crypto/md5"
	"encoding/hex"
	"errors"
	"fmt"
	"html"
	"log"
	"math/rand/v2"
	"os"
	"strings"
	"time"

	"github.com/badoux/checkmail"
	"gitlab.com/hackaton4.0Ranking/rest-api/api/auth"
	"gitlab.com/hackaton4.0Ranking/rest-api/api/email"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

type User struct {
	ID        uint32    `gorm:"primary_key;auto_increment" json:"id"`
	Nickname  string    `gorm:"size:255;not null;unique" json:"nickname"`
	Email     string    `gorm:"size:100;not null;unique" json:"email"`
	Password  string    `gorm:"size:100;not null;" json:"password,omitempty"`
	Admin			bool      `gorm:"not null;default:false" json:"admin"`
	CreatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	UpdatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
}

type LoginUser struct {
	User
	Token  string    `json:"token,omitempty"`
}

func Hash(password string) ([]byte, error) {
	return bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
}

func VerifyPassword(hashedPassword, password string) error {
	return bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(password))
}

func GetMD5Hash(text string) string {
   hash := md5.Sum([]byte(text))
   return hex.EncodeToString(hash[:])
}

func (u *User) BeforeSave(*gorm.DB) error {
	hashedPassword, err := Hash(u.Password)
	if err != nil {
		return err
	}
	u.Password = string(hashedPassword)
	return nil
}

func (u *User) Prepare() {
	u.ID = 0
	u.Nickname = html.EscapeString(strings.TrimSpace(u.Nickname))
	u.Email = html.EscapeString(strings.TrimSpace(u.Email))
	u.CreatedAt = time.Now()
	u.UpdatedAt = time.Now()
}

func (u *LoginUser) Validate(action string) error {
	switch strings.ToLower(action) {
	case "login":
		if u.Token == "" {
			if u.Password == "" {
				return errors.New("required Password")
			}
			if u.Email == "" {
				return errors.New("required Email")
			}
			if err := checkmail.ValidateFormat(u.Email); u.Email != "" && err != nil {
				return errors.New("invalid Email")
			}
		}
	}
	return nil
}

func (u *User) Validate(action string) error {
	switch strings.ToLower(action) {
	case "reset_password":
		return nil
	case "update":
		if u.Nickname == "" && u.Password == "" && u.Email == "" {
			return errors.New("required Nickname, Email or Password")
		}
		if u.Email != "" {
			if err := checkmail.ValidateFormat(u.Email); err != nil {
				return errors.New("invalid Email")
			}
		}

		return nil
	case "login":
		if u.Password == "" {
			return errors.New("required Password")
		}
		if u.Email == "" {
			return errors.New("required Email")
		}
		if err := checkmail.ValidateFormat(u.Email); err != nil {
			return errors.New("invalid Email")
		}
		return nil

	default:
		if u.Nickname == "" {
			return errors.New("required Nickname")
		}
		if u.Password == "" {
			return errors.New("required Password")
		}
		if u.Email == "" {
			return errors.New("required Email")
		}
		if err := checkmail.ValidateFormat(u.Email); err != nil {
			return errors.New("invalid Email")
		}
		return nil
	}
}

func (u *User) SaveUser(db *gorm.DB) (*User, error) {
	u.Password = GetMD5Hash(fmt.Sprintf("%d%d", rand.IntN(1000000),rand.IntN(1000000)))
	password := u.Password
	fmt.Println("Email: ", u.Email)
	fmt.Println("Password: ", u.Password)
	err := db.Debug().Create(&u).Error
	if err != nil {
		return &User{}, err
	}
	u.Password = ""
	e := email.Email{
		To: u.Email,
		Subject: "Login to Hackaton Ranking",
		Body: "user: " + u.Email + "<br>password: " + password,
	}
	err = e.Send()
	if err != nil {
		fmt.Println("Error sending email: ", err)
	}
	return u, nil
}

func (u *User) FindAllUsers(db *gorm.DB) (*[]User, error) {
	users := []User{}
	err := db.Debug().Model(&User{}).Omit("password").Limit(100).Find(&users).Error
	if err != nil {
		return &[]User{}, err
	}
	return &users, err
}

func (u *User) FindUserByID(db *gorm.DB, uid uint32) (*User, error) {
	err := db.Debug().Model(User{}).Where("id = ?", uid).Take(&u).Error
	if err != nil {
		return &User{}, err
	}
	if errors.Is(err, gorm.ErrRecordNotFound) {
		return &User{}, errors.New("User Not Found")
	}
	u.Password = ""
	return u, err
}

func (u *User) UpdateAUser(db *gorm.DB, uid uint32) (*User, error) {
	cols := map[string]interface{}{
		"updated_at": time.Now(),
	}
	if u.Password != "" {
		err := u.BeforeSave(db)
		if err != nil {
			log.Fatal(err)
		}
		cols["password"] = u.Password
	}
	if u.Nickname != "" {
		cols["nickname"] = u.Nickname
	}
	if u.Email != "" {
		cols["email"] = u.Email
	}
	db = db.Debug().Model(&User{}).Where("id = ?", uid).UpdateColumns(
		cols,
	)
	if db.Error != nil {
		return &User{}, db.Error
	}
	// This is the display the updated user
	err := db.Debug().Model(&User{}).Where("id = ?", uid).Take(&u).Error
	if err != nil {
		return &User{}, err
	}
	u.Password = ""
	return u, nil
}

func (u *User) ResetUserPassword(db *gorm.DB, uid uint32, sendEmail bool) (*User, error) {
	u.Password = GetMD5Hash(fmt.Sprintf("%d%d", rand.IntN(1000000),rand.IntN(1000000)))
	password := u.Password
	fmt.Println("Email: ", u.Email)
	fmt.Println("Password: ", u.Password)
	// To hash the password
	err := u.BeforeSave(db)
	if err != nil {
		log.Fatal(err)
	}
	db = db.Debug().Model(&User{}).Where("id = ?", uid).UpdateColumns(
		map[string]interface{}{
			"password":  u.Password,
		},
	)
	if db.Error != nil {
		return &User{}, db.Error
	}
	// This is the display the updated user
	err = db.Debug().Model(&User{}).Where("id = ?", uid).Take(&u).Error
	if err != nil {
		return &User{}, err
	}
	if sendEmail {
		token, err := auth.CreateLoginToken(u.Email, password)
		if err == nil {
			fmt.Println("URL: ", os.Getenv("FRONTEND_BASEURL") + os.Getenv("FRONTEND_LOGIN_PATH") + token)
			e := email.Email{
				To: u.Email,
				Subject: "Login to Hackaton Ranking",
				Body: "Hackaton Ranking: " + os.Getenv("FRONTEND_BASEURL") + os.Getenv("FRONTEND_LOGIN_PATH") + token,
			}
			err = e.Send()
			if err != nil {
				fmt.Println("Error sending email: ", err)
			}
		}
	}
	u.Password = ""
	return u, nil
}

func (u *User) DeleteAUser(db *gorm.DB, uid uint32) (int64, error) {

	db = db.Debug().Model(&User{}).Where("id = ?", uid).Delete(&User{})

	if db.Error != nil {
		return 0, db.Error
	}
	return db.RowsAffected, nil
}
